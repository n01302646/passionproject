﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;
using System.Web.Mvc;
using System.Data.SqlClient;
using PassionProject.Models;
using System.Diagnostics;
using System.IO;
using System.Net;
using PassionProject.Models.ViewModels;

namespace PassionProject.Controllers
{
    public class TeamsController : Controller
    {
        private PlayersCMScontext db = new PlayersCMScontext();
        // GET: Teams
        public ActionResult Index()
        {
            return View(db.team.ToList());
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add([Bind(Include = "TeamId,TeamName,TeamCountry,CoachName,TeamMotto")] Teams team)
        {
            if (ModelState.IsValid)
            {
                db.team.Add(team);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(team);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Teams team = db.team.Find(id);
            if (team == null)
            {
                return HttpNotFound();
            }
            return View(team);
        }
        public ActionResult Edit(int id)
        {
            TeamEdit team  = new TeamEdit();
            team.teams = db.team.Find(id);
            return View(team);
        }

        [HttpPost]
        public ActionResult Edit(int id, string TeamName, string TeamCountry, string CoachName, string TeamMotto)
        {
            if (db.team.Find(id) == null)
            {
                return HttpNotFound();
            }
            string query = "update teams set TeamName=@name, TeamCountry=@country, CoachName = @coach, TeamMotto=@teamid where teamId=@id";
            SqlParameter[] myparams = new SqlParameter[5];
            myparams[0] = new SqlParameter("@name", TeamName);
            myparams[1] = new SqlParameter("@country", TeamCountry);
            myparams[2] = new SqlParameter("@coach", CoachName);
            myparams[3] = new SqlParameter("@teamid", TeamMotto);
            myparams[4] = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Index");
        }


        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.team.Find(id) == null))
            {
                return HttpNotFound();

            }
         
            string queryPlayers = "delete from players where teamId_teamId = @id";
            SqlParameter paramplayers = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(queryPlayers, paramplayers);

            string query = "delete from teams where teamId = @id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return RedirectToAction("Index");
        }


    }
}