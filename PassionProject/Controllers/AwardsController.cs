﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;
using System.Web.Mvc;
using System.Data.SqlClient;
using PassionProject.Models;
using System.Diagnostics;
using System.IO;


namespace PassionProject.Controllers
{
    public class AwardsController : Controller
    {
        private PlayersCMScontext db = new PlayersCMScontext();
        // GET: Awards
        public ActionResult Index()
        {
            return View(db.award.ToList());
        }
    }
}