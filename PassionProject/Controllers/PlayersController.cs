﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.Entity;
using System.Web.Mvc;
using System.Data.SqlClient;
using PassionProject.Models;
using PassionProject.Models.ViewModels;
using System.Diagnostics;
using System.IO;
using System.Net;

namespace PassionProject.Controllers
{
    public class PlayersController : Controller
    {
        // GET: Players
        private PlayersCMScontext db = new PlayersCMScontext();
        public ActionResult Index()
        {
            return View(db.player.ToList());
        }

        public ActionResult Add()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Add(string PlayerName,string PlayerCountry,string PlayerRole)
        {
            string query = "insert into players (PlayerName, PlayerCountry, PlayerRole, TeamId_TeamId) values (@name, @country, @role,1)";
            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@name", PlayerName);
            myparams[1] = new SqlParameter("@country", PlayerCountry);
            myparams[2] = new SqlParameter("@role", PlayerRole);
           db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Index");
        }

        public ActionResult Edit(int id)
        {
            PlayerEdit playerEditView = new PlayerEdit();
            playerEditView.teams = db.team.ToList();           
            playerEditView.players = db.player.Find(id);
            return View(playerEditView);
        }

        [HttpPost]
        public ActionResult Edit(int id, string PlayerName, string PlayerCountry, string PlayerRole, FormCollection form)
        {
            if (db.player.Find(id) == null)
            {
                return HttpNotFound();
            }
            string teamId_TeamId = form["teamddl"];        
           
            string query = "update players set PlayerName=@name, PlayerCountry=@country, PlayerRole = @role, TeamId_TeamId=@teamid where PlayerId=@id";
            SqlParameter[] myparams = new SqlParameter[5];
            myparams[0] = new SqlParameter("@name", PlayerName);
            myparams[1] = new SqlParameter("@country", PlayerCountry);
            myparams[2] = new SqlParameter("@role", PlayerRole);
            myparams[3] = new SqlParameter("@teamid", teamId_TeamId);
            myparams[4] = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Index");
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Players player = db.player.Find(id);
            if (player == null)
            {
                return HttpNotFound();
            }
            return View(player);
        }

        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.player.Find(id) == null))
            {
                return HttpNotFound();

            }
            string query = "delete from players where PlayerId = @id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return RedirectToAction("Index");
        }

    }
}