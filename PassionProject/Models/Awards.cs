﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace PassionProject.Models
{
    public class Awards
    {
        [Key]
        public int AwardId { get; set;}

        [Required, StringLength(255), Display(Name = "AwardName")]
        public string AwardName { get; set;}

        [Required, StringLength(255), Display(Name = "PlayerYear")]
        public string AwardYear { get; set;}

        public virtual Players PlayerId { get; set; }
    }
}