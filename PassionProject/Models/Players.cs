﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
namespace PassionProject.Models
{
    public class Players
    {
        [Key]
        public int PlayerId { get; set; }

        [Required, StringLength(255), Display(Name = "PlayerName") ]
        public string PlayerName { get; set; }

        [Required, StringLength(255), Display(Name = "PlayerCountry")]
        public string PlayerCountry { get; set; }

        [Required, StringLength(255), Display(Name = "PlayerRole")]
        public string PlayerRole { get; set; }

        public virtual Teams TeamId { get; set; }
    }
}   