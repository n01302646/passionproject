﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace PassionProject.Models
{
    public class PlayersCMScontext : DbContext
    {

        public PlayersCMScontext()
        {

        }
        public DbSet<Players> player { get; set; }
        public DbSet<Teams> team { get; set; }
        public DbSet<Awards> award { get; set; }
    }
}