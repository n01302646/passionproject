﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionProject.Models.ViewModels
{
    public class PlayerEdit
    {
        //Empty constructor
        public PlayerEdit()
        {

        }

        //To edit a blog, you also need to pick from a list of authors

        public virtual Players players { get; set; }

        public IEnumerable<Teams> teams { get; set; }
    }
}