﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class Teams
    {
        [Key]
        public int TeamId { get; set; }

        [Required, StringLength(255), Display(Name = "TeamName")]
        public string TeamName { get; set; }

        [Required, StringLength(255), Display(Name = "CountryName")]
        public string TeamCountry { get; set; }

        [Required, StringLength(255), Display(Name = "CoachName")]
        public string CoachName { get; set; }

        [Required, StringLength(255), Display(Name = "TeamMotto")]
        public string TeamMotto { get; set; }

        public virtual ICollection<Players> playerId{ get; set; }
    }
}